# C'est quoi ?
Script permettant de créer une ISO afin d'installer archlinux avec l'environnement cinnamon, et divers logiciels libres pré-installés réputés et configurés. Le système après l'installation est "prêt à l'emploi".

# Installation
Il est important de décompresser ce projet dans votre dossier personnel.
Vous devez avoir l'arborescence $HOME > ISO > archecho ...

# Creation de l'ISO

Ouvrez un terminal dans le dossier gen > git > arco-cinnamon > installation-scripts.
Tapez ./30-build-the-iso-the-first-time.sh
(Touche TABulation pour l'autocomplétion)

Vous devrez mettre votre mot de passe administrateur. Le script va générer votre ISO dans le dossier HOME > ArcoLinuxB-Out avec ses fichiers d'empreintes.

# Mes modifications

L'installation des logiciels se fait en les renseignant dans le fichier packages.x86_64 dans le dossier arco-cinnamon > archiso

Ici, Gimp, jami, darktable, audacious, firefox, thunderbird, libreoffice... au début du fichiers sont renseignés pour les installer directement dans l'ISO. Vous pouvez les enlever, ou en rajouter.

Firefox est fourni avec un profil doté de quelques extensions comme ublok-origin, cookie-autodelete, Privacy-badger, yay! another speed dial, facebook-container, clearURLs, user-agent switcher... Startpage est utilisé comme moteur de recherche par défaut.

Un terminal s'ouvrira automatiquement à la première connexion afin d'installer les mises à jour, et youtube-dl en mode manuel, et faire un peu de nettoyage. Votre session redémarrera et c'est prêt.

Un thème est des configurations diverses (panel, menu) avec dconf sont effectués pour tous les utilisateurs de la machine.

J'ai inclus mon script archupdate (https://codeberg.org/echolib/arch-update) et PS1_prompt (https://codeberg.org/echolib/PS1_Prompt_Bash), présents sur ce compte.

# Images
Des images d'exemples sont présentes dans ce dépôt

# Bootez l'ISO
Gravez l'ISO et bootez dessus, ou essayez là dans un machine virtuelle comme QEMU. L'installation se passe par calamares, et facilite grandement l'installation de son système. Calamares permet d'ajouter des logiciels lors de l'installation, mais ils sont déjà inclus ou facilement ajoutables, comme mentionné ci-dessus. Vous pouvez donc passer ces étapes. Sélectionnez seulement le type de noyau que vous désirez.

Enjoy !

# Remerciements
Merci au projet arcolinux (https://arcolinux.com/) de rendre accessible l'installation d'archlinux.



